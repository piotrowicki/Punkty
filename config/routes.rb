Rails.application.routes.draw do
  get 'statistics/index'

  get 'repairs/new'

  get 'repairs/show'

  get 'sessions/new'

  

  resources :operators
  resources :points
  resources :statistics
  

  resources :users
  resources :repairs

  resources :sessions
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  root 'operators#index'
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'
  get '/punkty' => 'points#index'
  
  
  #logowanie----------------------
  get "log_out", to: "sessions#destroy", :as => "log_out"
  get "log_in", to:  "sessions#new", :as => "log_in"
  get "sign_up", to:  "users#new", :as => "sign_up"
  get "session/:id", to:  "sessions#change"
  
  
  
 
  #repairs------------------------
  get "/points/:point_id/repairs/new", to: "repairs#new"
  #get "/points/:point_id/repairs/:id/edit", to: "repairs#edit"
  get "/points/:point_id/repairs/:id", to: "repairs#show"
  get "/points/:point_id/repairs/:id", to: "repairs#destroy"
  
  #search-------------------------------------
  
  
  resources :points do
    resources :repairs
  end
  
  resources :operators do
    resources :points
  end
  
  

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase
  
  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end


  
  
  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
