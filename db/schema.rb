# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140723134625) do

  create_table "operators", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "telephone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "points", force: true do |t|
    t.string   "address"
    t.string   "town"
    t.string   "email"
    t.string   "telephone"
    t.date     "started_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "operator_id"
    t.string   "owner"
    t.string   "ip_address"
    t.string   "ip_printer"
    t.string   "ip_pos"
  end

  add_index "points", ["operator_id"], name: "index_points_on_operator_id", using: :btree

  create_table "repairs", force: true do |t|
    t.text     "description"
    t.integer  "user_id"
    t.integer  "point_id"
    t.boolean  "status"
    t.datetime "date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "repairs", ["point_id"], name: "index_repairs_on_point_id", using: :btree
  add_index "repairs", ["user_id"], name: "index_repairs_on_user_id", using: :btree

  create_table "sessions", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.string   "first_name"
    t.string   "last_name"
  end

end
