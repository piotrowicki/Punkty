class CreateRepairs < ActiveRecord::Migration
  def change
    create_table :repairs do |t|
      t.text :description
      t.references :user, index: true
      t.references :point, index: true
      t.boolean :status
      t.datetime :date

      t.timestamps
    end
  end
end
