class CreateOperators < ActiveRecord::Migration
  def change
    create_table :operators do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :telephone

      t.timestamps
    end
  end
end
