class AddOperatorRefToPoints < ActiveRecord::Migration
  def change
    #dodanie powiazania
    #rails g migration AddOperatorRefToPoints operator:references
    #dodaje kolumne operator_id do Point z indexem _id
    add_reference :points, :operator, index: true
  end
end
