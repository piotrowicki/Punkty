class AddIpAddressToPoints < ActiveRecord::Migration
  def change
    add_column :points, :ip_address, :string
  end
end
