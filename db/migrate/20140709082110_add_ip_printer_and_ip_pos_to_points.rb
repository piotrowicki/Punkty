class AddIpPrinterAndIpPosToPoints < ActiveRecord::Migration
  def change
    add_column :points, :ip_printer, :string
    add_column :points, :ip_pos, :string
  end
end
