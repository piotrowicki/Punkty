class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.string :address
      t.string :town
      t.string :email
      t.string :telephone
      t.date :started_at

      t.timestamps
    end
  end
end
