class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  
  helper_method  :current_user

  
  protect_from_forgery with: :exception
  
  
  #Autentykacja
  def authenticate
    #authenticate_or_request_with_http_basic do |username, password|
    # username == "admin" && password == "test4321"
    #end
    if current_user.nil?
      flash[:error] = 'You must be signed in to view that page.'
      redirect_to new_session_path
    end
  end
 
  #sprawdz czy istnieje sessja uzytkownika i przypisz ja do @current_user
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
 
 
  #app/controllers/application_controller.rb
  def page_not_found
    respond_to do |format|
      format.html { render template: 'errors/not_found_error', layout: 'layouts/application', status: 404 }
      format.all  { render nothing: true, status: 404 }
    end
  end
  
  
  
 
end
