class RepairsController < ApplicationController
  before_action :set_repair, only: [:show, :edit, :update, :destroy]
  
  #before action dla zagniezdzonych formularzy wyszukuje point
  before_action :set_point
  before_filter :authenticate, except: [:index, :show]
  
  
  helper_method :sort_column, :sort_direction


  # GET /repairs
  # GET /repairs.json
  def index
    
    @repairs = Point.find(params[:point_id]).repairs.paginate(:per_page => 20, :page => params[:page]).order('date DESC').search(params[:search])
    #@repairs = Repair.all 
  end
  

  # GET /repairs/1
  # GET /repairs/1.json
  def show
    
  end
  


  
  def sort_column
    Repair.column_names.include?(params[:sort]) ? params[:sort] : "first_name"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end



  # GET /repairs/new
  def new
    #@point = Point.find(params[:point_id])
    @repair = @point.repairs.build  
  end

  # GET /repairs/1/edit
  def edit
    
    
  end

  # POST /repairs
  # POST /repairs.json
  def create
    @repair = @point.repairs.build(repair_params)
    #@repair = 66
    
    respond_to do |format|
      if @repair.save
        #metoda show
        #format.html { redirect_to [@point,@repair], notice: 'Repair was successfully created.' }
        
        #metoda index repairs
        format.html { redirect_to point_repairs_path(@point), notice: 'Repair was successfully created.' }
        format.json { render :show, status: :created, location: @repair }
      else
        format.html { render :new}
        format.json { render json: @repair.errors, status: :unprocessable_entity }       
      end    
    end
  end

  # PATCH/PUT /repairs/1
  # PATCH/PUT /repairs/1.json
  def update
 
    respond_to do |format|
      if @repair.update(repair_params)
        format.html { redirect_to [@point,@repair], notice: 'Repair was successfully updated.' }
        format.json { render :show, status: :ok, location: @repair }
      else
        format.html { render :edit }
        format.json { render json: @repair.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /repairs/1
  # DELETE /repairs/1.json
  def destroy
    
    @repair.destroy
    respond_to do |format|
      format.html { redirect_to point_repairs_url(@point), notice: 'Repair was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_repair
    @repair = Repair.find(params[:id])
  end
  
   def set_point
      @point = Point.find(params[:point_id])
    end

  # Never trust parameters from the scary internet, only allow the white list through.
  def repair_params
    params.require(:repair).permit(:description, :point_id, :user_id, :status, :date)
  end
  
  
end
