class PointsController < ApplicationController
  before_action :set_point, only: [:show, :edit, :update, :destroy]
  #before_action :validate, only: :name -- nie wiem co to jest
  
  
  #Przed wywolaniem wykonaj instrukcje autentykacji w application controller
  before_filter :authenticate, except: [:index, :show]
  
  helper_method :sort_column, :sort_direction
  
  # GET /points
  # GET /points.json
  # Do index dołączona paginacja i metoda wyszukująca
  def index
    if params[:operator_id]
      @points = Operator.find(params[:operator_id]).points.paginate(:per_page => 20, :page => params[:page]).search(params[:search],params[:column])
    else
      @points = Point.joins(:operator).paginate(:per_page => 20, :page => params[:page]).search(params[:search],params[:column]).order(sort_column + " " + sort_direction)
    end
  end
   
  def sort_column
    Point.column_names.include?(params[:sort]) ? params[:sort] : "first_name"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  
  # GET /points/1
  # GET /points/1.json
  def show
  end

  # GET /points/new
  def new
    @point = Point.new
  end

  # GET /points/1/edit
  def edit
  end

  # POST /points
  # POST /points.json
  def create
    @point = Point.new(point_params)
    
    respond_to do |format|
      if @point.save
        format.html { redirect_to @point, notice: 'Point was successfully created.' }
        format.json { render :show, status: :created, location: @point }
      else
        format.html { render :new }
        format.json { render json: @point.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /points/1
  # PATCH/PUT /points/1.json
  def update
    respond_to do |format|
      if @point.update(point_params)
        format.html { redirect_to @point, notice: 'Point was successfully updated.' }
        format.json { render :show, status: :ok, location: @point }
      else
        format.html { render :edit }
        format.json { render json: @point.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /points/1
  # DELETE /points/1.json
  def destroy
    @point.destroy
    respond_to do |format|
      format.html { redirect_to points_url, notice: 'Point was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_point
    @point = Point.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def point_params
    params.require(:point).permit(:address, :town, :email, :telephone, :started_at, :operator_id, :owner,:ip_address, :point_id, :ip_pos, :ip_printer)
  end
end
