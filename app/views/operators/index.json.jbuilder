
json.array!(@operators) do |operator|
  json.extract! operator, :id, :first_name, :last_name, :email, :telephone
  json.url operator_url(operator, format: :json)
end
