json.array!(@points) do |point|
  json.extract! point, :id, :address, :town, :email, :telephone, :started_at
  json.url point_url(point, format: :json)
end
