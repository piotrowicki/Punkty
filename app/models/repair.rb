class Repair < ActiveRecord::Base
  belongs_to :user
  belongs_to :point
  
  validates_presence_of :description
  validates :status, :inclusion => { :in => [true, false] }
  
  
  #Metoda wyszukująca napisana z palca
  def self.search(search)
    if search 
      #CALOSC PO WSZYSTKICH KOLUMNACH
      Repair.joins(:user).where('email LIKE ? OR description LIKE ? OR date LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%") 
   
    else 
      Repair.all
    end
  end
end
