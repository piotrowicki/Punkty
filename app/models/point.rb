class Point < ActiveRecord::Base  
  belongs_to :operator
  has_many :repairs, :dependent => :destroy
  
  validates_presence_of :address, :town
  validates_uniqueness_of :address
 
  #do_not_validate_attachment_file_type :photo
  #do_not_validate_attachment_file_type :photo
  
 
  #paperclip gem dodajacy/wyswietlajacy zdjecia
  
  #Metoda wyszukująca napisana z palca
  def self.search(search, column)
    if search 
      #points=Point.arel_table
     
      case column
      when "1"
        Point.where("last_name LIKE ?", "%#{search}%")
      when "2"
        Point.where("address LIKE ?", "%#{search}%")
      when "3"
        Point.where("town LIKE ?", "%#{search}%")
      else
        Point.all 
      end
      #errors.add(:search, "Please type in the correct email address.")
  
      
      #CALOSC PO WSZYSTKICH KOLUMNACH
      #Point.joins(:operator).where('last_name LIKE ? OR address LIKE ? OR town LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%") 
   
    else 
      Point.all
    end
  end
end

