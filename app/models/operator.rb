class Operator < ActiveRecord::Base
  has_many :points, :dependent => :destroy
  
  validates_presence_of  :last_name, :first_name
  #validates_numericality_of :telephone
end
